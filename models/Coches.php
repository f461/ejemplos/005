<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coches".
 *
 * @property int $id
 * @property string|null $marcha
 * @property string|null $modelo
 * @property float|null $precio
 * @property string|null $fecha_entrada
 * @property string|null $foto
 * @property int|null $cilindrada
 */
class Coches extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'cilindrada'], 'integer'],
            [['precio'], 'number'],
            [['fecha_entrada'], 'safe'],
            [['marcha', 'modelo', 'foto'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marcha' => 'Marcha',
            'modelo' => 'Modelo',
            'precio' => 'Precio',
            'fecha_entrada' => 'Fecha Entrada',
            'foto' => 'Foto',
            'cilindrada' => 'Cilindrada',
        ];
    }
}
